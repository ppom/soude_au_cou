use std::convert::From;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io;

/// Sudoku Error, used inside this module
#[derive(Debug)]
pub enum SudokuError {
    IndexOutOfRange,
    ValueOutOfRange,
    FixedCell,
    OpenDisplay,
    LittleDisplay,
    IoError(io::Error),
}

impl Display for SudokuError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                SudokuError::IndexOutOfRange => "Bornes du sudoku dépassées".into(),
                SudokuError::ValueOutOfRange => "Chiffre en dehors des bornes".into(),
                SudokuError::FixedCell => "Cette case ne peut pas être modifiée".into(),
                SudokuError::OpenDisplay => "Ne peut pas ouvrir le terminal".into(),
                SudokuError::LittleDisplay => "Le terminal est trop petit".into(),
                SudokuError::IoError(ie) => format!("Erreur d'entrée/sortie: {}", ie),
            }
        )
    }
}

impl Error for SudokuError {}

impl From<io::Error> for SudokuError {
    fn from(error: io::Error) -> Self {
        SudokuError::IoError(error)
    }
}
