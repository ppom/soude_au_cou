use std::fmt::Display;
use std::io::{self, Error as IoError, Write};
use std::ops::Drop;

// use std::thread;
// use std::time::Duration;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::{clear, color, cursor, style};

use super::engine::{State, Sudoku};
use super::error::SudokuError;

// HELP

pub fn print_help() {
    println!(
        "CONTROLS
use `control c`, `control d` or `q` to quit.
use `hjkl` or `←↓↑→` to move.
use `space` or `0` to clear a cell.
use `123456789` to fill a cell.
    (azerty keyboards: no need to press shift)

LICENSE
This software is published under the Blue Oak v1.0.0 license, see https://blueoakcouncil.org/license/1.0.0

soudaucou version 0.1.0"

    );
}

// MENU UI

pub fn starting_menu() {
    match _starting_menu() {
        Ok(()) => {}
        Err(e) => println!("Erreur fatale : {}", e),
    }
}

fn _starting_menu() -> Result<(), SudokuError> {
    let mut choice = 0;
    {
        let mut stdout = std::io::stdout().into_raw_mode()?;
        write!(
            stdout,
            "{}{}{}{}{}",
            clear::All,
            cursor::Hide,
            color::Fg(color::Reset),
            color::Bg(color::Reset),
            style::Reset
        )?;
        let stdin = io::stdin();
        let mut stdin = stdin.lock().keys();
        print_menu_cursor(&mut stdout, choice + 2)?;
        stdout.flush()?;
        loop {
            write!(
            stdout,
            "{}{}SUDOKU{}{}Partie test{}Partie facile{}Partie normale{}Partie difficile{}Partie Ninja",
            style::Bold,
            cursor::Goto(1, 1),
            style::NoBold,
            cursor::Goto(3, 2),
            cursor::Goto(3, 3),
            cursor::Goto(3, 4),
            cursor::Goto(3, 5),
            cursor::Goto(3, 6),
        )?;
            stdout.flush()?;
            if let Some(Ok(key)) = stdin.next() {
                match key {
                    Key::Down | Key::Char('j') => {
                        if choice < 4 {
                            choice = choice + 1
                        }
                    }
                    Key::Up | Key::Char('k') => {
                        if choice > 0 {
                            choice = choice - 1
                        }
                    }
                    Key::Char('\n') => break,
                    _ => {
                        write!(stdout, "{}", cursor::Show)?;
                        return Ok(());
                    }
                }
            }
            print_menu_cursor(&mut stdout, choice + 2)?;
            stdout.flush()?;
        }
    }
    let holes = match choice {
        0 => 1,
        1 => 35,
        2 => 45,
        3 => 55,
        4 | _ => 80,
    };
    run_one_game(holes);
    write!(std::io::stdout(), "{}", cursor::Show)?;
    Ok(())
}

fn print_menu_cursor(stdout: &mut io::Stdout, line: u8) -> Result<(), SudokuError> {
    write!(
        stdout,
        "{} {} {} {} {} {}{}>",
        cursor::Goto(1, 2),
        cursor::Goto(1, 3),
        cursor::Goto(1, 4),
        cursor::Goto(1, 5),
        cursor::Goto(1, 6),
        cursor::Goto(1, line as u16),
        style::Bold,
    )?;
    Ok(())
}

pub fn run_one_game(holes: u8) {
    match run(holes) {
        Ok(End::Success) => println!(
            "{}Tu as {}fini {}le su{}doku {}jusq{}u'au {}bout",
            color::Rgb(148, 0, 11).fg_string(),
            color::Rgb(75, 0, 130).fg_string(),
            color::Rgb(0, 0, 255).fg_string(),
            color::Rgb(0, 255, 0).fg_string(),
            color::Rgb(255, 255, 0).fg_string(),
            color::Rgb(255, 127, 0).fg_string(),
            color::Rgb(255, 0, 0).fg_string()
        ),
        Ok(End::Abort) => {
            print!("Tu n'as pas fini le sudoku jusqu'au bout.");
            /*
            std::io::stdout().flush().unwrap();
            thread::sleep(Duration::from_millis(2000));
            print!(" Petite merde.");
            std::io::stdout().flush().unwrap();
            thread::sleep(Duration::from_millis(150));
            println!("\rTu n'as pas fini le sudoku jusqu'au bout.                  ");
            */
        }
        Err(e) => println!("Erreur fatale : {}", e),
    };
}

// SUDOKU UI

pub fn run(holes: u8) -> Result<End, SudokuError> {
    // TODO if termion::is_tty
    let mut tui = TUI::new()?;
    let mut grid = Sudoku::new(Some(holes));
    let mut last_wrong_numbers: Vec<(u8, u8)> = Vec::new();
    tui.print_numbers(&grid, &last_wrong_numbers)?;
    let stdin = io::stdin();
    let stdin = stdin.lock();
    for key in stdin.keys() {
        tui.reset_if_changed(&grid, &last_wrong_numbers)?;
        match key {
            Err(e) => {
                tui.error_present(Some(e))?;
            }
            Ok(key) => match key {
                // Quit
                Key::Ctrl('c') | Key::Ctrl('d') | Key::Char('q') => return Ok(End::Abort),
                Key::Char('h') | Key::Left => {
                    tui.move_left()?;
                    tui.error_present::<bool>(None)?;
                }
                Key::Char('l') | Key::Right => {
                    tui.move_right()?;
                    tui.error_present::<bool>(None)?;
                }
                Key::Char('k') | Key::Up => {
                    tui.move_up()?;
                    tui.error_present::<bool>(None)?;
                }
                Key::Char('j') | Key::Down => {
                    tui.move_down()?;
                    tui.error_present::<bool>(None)?;
                }
                Key::Char(' ') | Key::Backspace | Key::Char('0') | Key::Char('à') => {
                    if let Some(result) = set(&mut grid, &mut tui, &mut last_wrong_numbers, 0)? {
                        return result;
                    }
                }
                Key::Char(c) => {
                    if "&1é2\"3'4(5-6è7_8ç9".contains(c) {
                        let n = match c {
                            '&' | '1' => 1,
                            'é' | '2' => 2,
                            '"' | '3' => 3,
                            '\'' | '4' => 4,
                            '(' | '5' => 5,
                            '-' | '6' => 6,
                            'è' | '7' => 7,
                            '_' | '8' => 8,
                            'ç' | '9' => 9,
                            _ => panic!("nique ta pierre pour la fete des pierres"),
                        };
                        if let Some(result) = set(&mut grid, &mut tui, &mut last_wrong_numbers, n)?
                        {
                            return result;
                        }
                    } else {
                        tui.error_present(Some(format!("Unrecognized command: {}", c)))?;
                        // TODO autres commandes
                    }
                }
                Key::F(c) => tui.error_present(Some(format!("Unrecognized command: F{}", c)))?,
                Key::Alt(c) => {
                    tui.error_present(Some(format!("Unrecognized command: Alt+{}", c)))?
                }
                Key::Ctrl(c) => {
                    tui.error_present(Some(format!("Unrecognized command: Ctrl+{}", c)))?
                }
                _ => tui.error_present(Some(format!("Unrecognized command.")))?,
            },
        }
    }
    Ok(End::Abort)
}

fn set(
    grid: &mut Sudoku,
    tui: &mut TUI,
    last_wrong_numbers: &mut Vec<(u8, u8)>,
    n: u8,
) -> Result<Option<Result<End, SudokuError>>, SudokuError> {
    let (i, j) = tui.coord();
    match grid.set(i as usize, j as usize, n) {
        Err(e) => tui.error_present(Some(e))?,
        Ok(()) => {
            tui.print_number(i, j, n, false, false)?;
            tui.update_cursor()?;
            match grid.state() {
                State::UnfinishedCorrect => {
                    tui.error_present::<bool>(None)?;
                    last_wrong_numbers.clear();
                    tui.print_numbers(&grid, &last_wrong_numbers)?;
                }
                State::FinishedCorrect => {
                    return Ok(Some(Ok(End::Success)));
                }
                State::Incorrect(mut positions) => {
                    tui.error_present(Some("Doublons !"))?;
                    last_wrong_numbers.clear();
                    last_wrong_numbers.append(&mut positions);
                    tui.print_numbers(&grid, &last_wrong_numbers)?;
                }
            }
        }
    }
    Ok(None)
}

pub enum End {
    Success,
    Abort,
}

// TUI

pub const SUDOKU_WIDTH: u16 = 20;
pub const SUDOKU_HEIGHT: u16 = 11;

struct TUI {
    stdout: RawTerminal<io::Stdout>,
    columns: u16,
    lines: u16,
    line_start: u16,
    column_start: u16,
    i: u8,
    j: u8,
}

impl Drop for TUI {
    fn drop(&mut self) {
        write!(
            self.stdout,
            "{}{}{}{}{}",
            cursor::Show,
            clear::All,
            style::Reset,
            style::NoUnderline,
            cursor::Goto(1, 1),
        )
        .unwrap();
        // can't do better than unwraping this write call ?
    }
}

impl TUI {
    fn new() -> Result<TUI, SudokuError> {
        let (columns, lines) = termion::terminal_size()?;
        let mut tui = TUI {
            stdout: io::stdout().into_raw_mode()?,
            columns,
            lines,
            line_start: 1,
            column_start: 1,
            i: 4,
            j: 4,
        };
        tui.reset()?;
        Ok(tui)
    }

    fn reset(&mut self) -> Result<(), SudokuError> {
        // clear
        write!(
            self.stdout,
            "{}{}{}{}{}",
            clear::All,
            cursor::Show,
            color::Fg(color::Reset),
            color::Bg(color::Reset),
            style::Reset
        )?;
        // update term size
        let (columns, lines) = termion::terminal_size()?;
        if lines < SUDOKU_HEIGHT + 2 || columns < SUDOKU_WIDTH {
            return Err(SudokuError::LittleDisplay);
        }
        self.columns = columns;
        self.lines = lines;
        // update TUI offset
        self.line_start = (lines - SUDOKU_HEIGHT) / 2;
        self.column_start = (columns - SUDOKU_WIDTH) / 2;
        // print grid
        self.print_grid()?;
        // print cursor
        self.update_cursor()?;
        // Flush stdout (i.e. make the output appear).
        self.stdout.flush()?;
        Ok(())
    }

    fn reset_if_changed(
        &mut self,
        sudoku: &Sudoku,
        last_wrong_numbers: &Vec<(u8, u8)>,
    ) -> Result<(), SudokuError> {
        let (columns, lines) = termion::terminal_size()?;
        if columns != self.columns || lines != self.lines {
            self.reset()?;
            self.print_numbers(&sudoku, &last_wrong_numbers)?;
        }
        Ok(())
    }

    fn error_present<D: Display>(&mut self, err: Option<D>) -> Result<(), IoError> {
        let line = self.line_start + SUDOKU_HEIGHT + 2;
        write!(
            self.stdout,
            "{}{}",
            cursor::Goto(self.column_start, line),
            clear::CurrentLine,
        )?;
        if let Some(err) = err {
            let err = err.to_string();
            let column;
            if (self.column_start as usize) > err.len() / 2 {
                column = self.column_start + SUDOKU_WIDTH / 2 - err.len() as u16 / 2;
            } else {
                column = 0;
            }
            write!(
                self.stdout,
                "{}{}{}{}{}",
                cursor::Goto(column, line),
                color::Red.fg_str(),
                style::NoBold,
                err,
                color::White.fg_str()
            )?;
        }
        self.update_cursor()?;
        self.stdout.flush()?;
        Ok(())
    }

    fn print_grid(&mut self) -> Result<(), IoError> {
        // print columns
        for i in 0..11 {
            write!(
                self.stdout,
                "{}│       │",
                cursor::Goto(self.column_start + 6, self.line_start + i)
            )?;
        }
        // print lines
        for i in 0..2 {
            write!(
                self.stdout,
                "{}──────┼───────┼──────",
                cursor::Goto(self.column_start, self.line_start + 3 + 4 * i)
            )?;
        }
        Ok(())
    }

    fn goto(&self, i: u8, j: u8) -> cursor::Goto {
        cursor::Goto(
            self.column_start + (j * 2 + 2 * (j / 3)) as u16,
            self.line_start + (i + i / 3) as u16,
        )
    }

    fn update_cursor(&mut self) -> Result<(), IoError> {
        write!(self.stdout, "{}", self.goto(self.i, self.j))?;
        self.stdout.flush()
    }

    fn print_number(
        &mut self,
        i: u8,
        j: u8,
        n: u8,
        fixed: bool,
        error: bool,
    ) -> Result<(), IoError> {
        write!(
            self.stdout,
            "{}{}{}{}",
            self.goto(i, j),
            if error {
                color::Red.fg_str()
            } else {
                color::White.fg_str()
            },
            // Bold and NoBold are two different structs and types need to match
            if fixed {
                style::NoBold.to_string()
            } else {
                style::Bold.to_string()
            },
            format!(
                "{}",
                match n {
                    0 => String::from(" "),
                    b => format!("{}", b),
                }
            )
        )
    }

    fn print_numbers(
        &mut self,
        grid: &Sudoku,
        last_wrong_numbers: &Vec<(u8, u8)>,
    ) -> Result<(), IoError> {
        for i in 0..(9 as u8) {
            for j in 0..(9 as u8) {
                let cell = grid
                    .get_cell(i as usize, j as usize)
                    .expect("Unexpected error in TUI::print_numbers");
                self.print_number(
                    i,
                    j,
                    cell.get(),
                    cell.is_fixed(),
                    last_wrong_numbers.contains(&(i, j)),
                )?;
            }
        }
        self.update_cursor()?;
        Ok(())
    }

    //     fn move_to(&mut self, i: u8, j: u8) -> Result<(), SudokuError> {
    //         if i > 8 || j > 8 {
    //             Err(SudokuError::ValueOutOfRange)
    //         } else {
    //             self.i = i;
    //             self.j = j;
    //             self.update_cursor()?;
    //             Ok(())
    //         }
    //     }

    fn move_up(&mut self) -> Result<(), IoError> {
        self.i = if self.i == 0 { 8 } else { self.i - 1 };
        self.update_cursor()
    }
    fn move_down(&mut self) -> Result<(), IoError> {
        self.i = if self.i == 8 { 0 } else { self.i + 1 };
        self.update_cursor()
    }

    fn move_left(&mut self) -> Result<(), IoError> {
        self.j = if self.j == 0 { 8 } else { self.j - 1 };
        self.update_cursor()
    }
    fn move_right(&mut self) -> Result<(), IoError> {
        self.j = if self.j == 8 { 0 } else { self.j + 1 };
        self.update_cursor()
    }

    //     fn set(&mut self, value: u8) -> Result<(), SudokuError> {
    //         if value > 9 {
    //             Err(SudokuError::ValueOutOfRange)
    //         } else {
    //             write!(self.stdout, "")?;
    //             Ok(())
    //         }
    //     }

    fn coord(&self) -> (u8, u8) {
        (self.i, self.j)
    }
}
