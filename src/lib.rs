// relatives
mod engine;
mod error;
mod perm;
mod tui;

pub use crate::engine::{Cell, State, Sudoku};
pub use crate::error::SudokuError as Error;
pub use crate::tui::{print_help, run as run_tui, run_one_game, starting_menu as menu, End};

#[allow(dead_code)]
pub fn benchmark() {
    let mut sum = 0;
    for _ in 0..100 {
        let s = Sudoku::new(None);
        println!("{}", s);
        let mut filled_cells = 0;
        for i in 0..9 {
            for j in 0..9 {
                if s.get(i, j).unwrap() != 0 {
                    filled_cells += 1;
                }
            }
        }
        println!("{}", filled_cells);
        sum += filled_cells;
    }
    println!();
    println!("mean: {}", sum / 100);
}

#[allow(dead_code)]
pub fn resolve_test() {
    let mut s = Sudoku::rand_template();
    for sudoku in s.resolve() {
        println!("1. Unique solution: \n{}", sudoku);
    }
    s.set(0, 0, 0).expect("main panic");
    s.set(0, 1, 0).expect("main panic");
    s.set(0, 2, 0).expect("main panic");
    println!("2. Emptied: \n{}", s);
    println!("3.Solutions:");
    for sudoku in s.resolve() {
        println!("3. Unique solution: \n{}", sudoku);
    }
    for i in 0..3 {
        for j in 0..9 - 3 {
            s.set(i, j, 0).expect("main panic");
        }
    }
    println!("\n4. Emptied: \n{}", s);
    println!("5.Solutions:");
    let sudokus = s.resolve();
    for sudoku in &sudokus {
        println!("5. Multiple solutions: \n{}", sudoku);
    }
    println!("number of solutions: {}", sudokus.len());
}
