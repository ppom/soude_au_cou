use soudaucou::{menu, print_help};
use std::env;

fn main() {
    let config = parse_args();
    if config.help {
        print_help();
    } else {
        menu();
    }
}

struct Config {
    help: bool,
}

fn parse_args() -> Config {
    let mut config = Config { help: false };
    let args = env::args();
    for arg in args {
        if arg == "-h" || arg == "--help" {
            config.help = true;
        }
    }
    config
}
