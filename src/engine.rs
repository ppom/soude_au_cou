// standard library
use std::collections::HashSet;
use std::fmt::{self, Display, Formatter};

// crates
use rand::{self, seq::SliceRandom, Rng};

// relatives
use super::error::SudokuError;
use super::perm::Perm;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Default)]
pub struct Cell {
    value: u8,
    fixed: bool,
}

impl Cell {
    fn new_unfixed(value: u8) -> Cell {
        Cell {
            value,
            fixed: false,
        }
    }
    pub fn get(&self) -> u8 {
        self.value
    }
    pub fn set(&mut self, value: u8) -> Result<(), SudokuError> {
        if value > 9 {
            Err(SudokuError::ValueOutOfRange)
        } else if self.fixed {
            Err(SudokuError::FixedCell)
        } else {
            self.value = value;
            Ok(())
        }
    }
    pub fn is_fixed(&self) -> bool {
        self.fixed
    }
    fn fix(&mut self) {
        self.fixed = true;
    }
}

/// The Sudoku struct
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Sudoku {
    grid: [[Cell; 9]; 9],
}

impl Display for Sudoku {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self._display())
    }
}

impl Sudoku {
    pub fn empty() -> Sudoku {
        Sudoku {
            grid: [[Cell::default(); 9]; 9],
        }
    }

    fn template() -> Sudoku {
        let mut grid = [[Cell::default(); 9]; 9];
        for i in 0..9 {
            for j in 0..9 {
                let headvalue = j % 3; // 0,1,2
                let basevalue = (i + j / 3) % 3; // 0,1,2
                let lastvalue = basevalue * 3 + (headvalue + i / 3) % 3 + 1; // 1,2,3,4,5,6,7,8,9
                grid[i as usize][j as usize] = Cell::new_unfixed(lastvalue);
            }
        }
        Sudoku { grid }
    }

    pub fn set(&mut self, i: usize, j: usize, value: u8) -> Result<(), SudokuError> {
        if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            self.grid[i][j].set(value)?;
            Ok(())
        }
    }
    fn set_column(&mut self, j: usize, column: [u8; 9]) -> Result<(), SudokuError> {
        for i in 0..9 {
            self.set(i, j, column[i])?;
        }
        Ok(())
    }
    fn set_line(&mut self, i: usize, column: [u8; 9]) -> Result<(), SudokuError> {
        for j in 0..9 {
            self.set(i, j, column[j])?;
        }
        Ok(())
    }

    pub fn get_cell(&self, i: usize, j: usize) -> Result<&Cell, SudokuError> {
        if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            Ok(&self.grid[i][j])
        }
    }
    pub fn get_cell_mut(&mut self, i: usize, j: usize) -> Result<&mut Cell, SudokuError> {
        if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            Ok(&mut self.grid[i][j])
        }
    }
    pub fn get(&self, i: usize, j: usize) -> Result<u8, SudokuError> {
        if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            Ok(self.grid[i][j].value)
        }
    }

    fn get_column(&self, j: usize) -> Result<[u8; 9], SudokuError> {
        if j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            let mut res = [0; 9];
            for i in 0..9 {
                res[i] = self.grid[i][j].value;
            }
            Ok(res)
        }
    }
    fn get_line(&self, i: usize) -> Result<[u8; 9], SudokuError> {
        if i >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            let mut res = [0; 9];
            for j in 0..9 {
                res[j] = self.grid[i][j].value;
            }
            Ok(res)
        }
    }
    /*
    fn get_square(&self, i: usize, j: usize) -> Result<[usize; 9], SudokuError> {
        if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            let i_start = i / 3;
            let j_start = j / 3;
            let mut res = [0; 9];
            for i_offset in 0..3 {
                for j_offset in 0..3 {
                    res[i_offset * 3 + j_offset] =
                        self.grid[i_start * 3 + i_offset][j_start * 3 + j_offset];
                }
            }
            Ok(res)
        }
    }
    */

    pub fn display(&self) {
        print!("{}", self._display());
    }

    fn _display(&self) -> String {
        let mut s = String::with_capacity(374);
        // 3 line blocks
        for i in 0..3 {
            // cesure line
            if i != 0 {
                s += "──────┼───────┼──────\n";
            }
            // 3 lines
            for j in 0..3 {
                // 3 column blocks
                for k in 0..3 {
                    // cesure column
                    if k != 0 {
                        s += "│ ";
                    }
                    // 3 columns
                    for l in 0..3 {
                        s += format!(
                            "{} ",
                            match self.grid[i * 3 + j][k * 3 + l].value {
                                0 => String::from(" "),
                                b => format!("{}", b),
                            }
                        )
                        .as_ref();
                    }
                }
                s += "\n";
            }
        }
        s += "\n";
        s
    }

    pub fn rand_template() -> Sudoku {
        let mut s = Sudoku::template();
        let perm = Perm::new(3);
        for block in 0..3 as u8 {
            // Permutate columns
            // TODO : no clone, only one 'pivot'
            let s_tmp = s.clone();
            let rand_perm = perm.rand_perm();
            for i in rand_perm {
                let index = *rand_perm
                    .get(*i as usize)
                    .expect("Panic 1 in Sudoku::full_rand");

                let column = s_tmp
                    .get_column((block * 3 + index) as usize)
                    .expect("Panic 2 in Sudoku::full_rand");

                s.set_column((block * 3 + *i) as usize, column)
                    .expect("Panic 3 in Sudoku::full_rand");
            }
            // Permutate lines
            let s_tmp = s.clone();
            let rand_perm = perm.rand_perm();
            for i in rand_perm {
                let index = *rand_perm
                    .get(*i as usize)
                    .expect("Panic 4 in Sudoku::full_rand");

                let line = s_tmp
                    .get_line((block * 3 + index) as usize)
                    .expect("Panic 5 in Sudoku::full_rand");

                s.set_line((block * 3 + *i) as usize, line)
                    .expect("Panic 6 in Sudoku::full_rand");
            }
        }
        s.shuffle_numbers();
        for i in 0..9 {
            for j in 0..9 {
                s.grid[i][j].fix();
            }
        }
        s
    }

    fn on_column(&self, j: usize, number: u8) -> Result<bool, SudokuError> {
        if number >= 9 + 1 {
            Err(SudokuError::ValueOutOfRange)
        } else if j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            for i in 0..9 {
                if self.grid[i][j].value == number {
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }

    fn on_line(&self, i: usize, number: u8) -> Result<bool, SudokuError> {
        if number >= 9 + 1 {
            Err(SudokuError::ValueOutOfRange)
        } else if i >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            for j in 0..9 {
                if self.grid[i][j].value == number {
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }

    fn on_square(&self, i: usize, j: usize, number: u8) -> Result<bool, SudokuError> {
        if number >= 9 + 1 {
            Err(SudokuError::ValueOutOfRange)
        } else if i >= 9 || j >= 9 {
            Err(SudokuError::IndexOutOfRange)
        } else {
            let i_start = i / 3;
            let j_start = j / 3;
            for i_offset in 0..3 {
                for j_offset in 0..3 {
                    if self.grid[i_start * 3 + i_offset][j_start * 3 + j_offset].value == number {
                        return Ok(true);
                    }
                }
            }
            Ok(false)
        }
    }

    #[allow(dead_code)]
    fn place_took(&self, i: usize, j: usize, number: u8) -> Result<bool, SudokuError> {
        Ok(self.on_line(i, number)?
            || self.on_column(j, number)?
            || self.on_square(i, j, number)?)
    }

    pub fn resolve(&self) -> HashSet<Sudoku> {
        // TODO : the sudoku needs to be mutable to modify it. But at the end it remains the same.
        // So it's not really mutable, no ? Now I just clone it to have a mutable struct to work with.
        self.clone()._resolve(0, 1)
    }

    fn _resolve(&mut self, index_start: usize, do_thread: u8) -> HashSet<Sudoku> {
        // catch next blank box
        let index = {
            // gain some time starting at the last offset
            let mut index = index_start;
            loop {
                if index >= 81 || self.grid[index / 9][index % 9].value == 0 {
                    break;
                }
                index += 1;
            }
            if index >= 81 {
                None
            } else {
                Some(index)
            }
        };
        let mut solutions = HashSet::new();
        match index {
            // Full sudoku => solution !
            None => {
                solutions.insert(self.clone());
            }
            // Not full sudoku => 0..n solutions
            Some(index) => {
                let i = index / 9;
                let j = index % 9;

                for number in 1..=9 {
                    if !self
                        .place_took(i, j, number)
                        .expect("Panic in Sudoku::resolve")
                    {
                        self.grid[i][j].value = number;

                        for solution in
                            self._resolve(index + 1, if do_thread == 0 { 0 } else { do_thread - 1 })
                        {
                            solutions.insert(solution);
                        }
                    }
                }
                self.grid[i][j].value = 0;
            }
        }
        solutions
    }

    fn resolve_one(&self) -> Option<Sudoku> {
        self.clone()._resolve_one(0)
    }

    fn _resolve_one(&mut self, index_start: usize) -> Option<Sudoku> {
        // catch next blank box
        let index = {
            // gain some time starting at the last offset
            let mut index = index_start;
            loop {
                if index >= 81 || self.grid[index / 9][index % 9].value == 0 {
                    break;
                }
                index += 1;
            }
            if index >= 81 {
                None
            } else {
                Some(index)
            }
        };
        // let mut solutions = HashSet::new();
        match index {
            // Full sudoku => solution !
            None => {
                return Some(self.clone());
                // solutions.insert(self.clone());
            }
            // Not full sudoku => 0..n solutions
            Some(index) => {
                let i = index / 9;
                let j = index % 9;

                let mut vec: Vec<u8> = (1..=9).collect();
                vec.shuffle(&mut rand::thread_rng());

                for number in vec {
                    if !self
                        .place_took(i, j, number)
                        .expect("Panic in Sudoku::resolve")
                    {
                        self.grid[i][j].value = number;

                        if let Some(ret) = self._resolve_one(index + 1) {
                            return Some(ret);
                        }
                    }
                }
                self.grid[i][j].value = 0;
            }
        }
        None
        // solutions
    }

    fn is_solvable(&self) -> bool {
        self.resolve().len() == 1
    }

    pub fn new(blanks: Option<u8>) -> Sudoku {
        // default for 20 blanks
        let mut blanks = match blanks {
            None => 81,
            Some(b) => b,
        };
        // vector of all removable cells
        let mut removable_cells = Vec::with_capacity(81);
        for i in 0..9 {
            for j in 0..9 {
                removable_cells.push((i, j));
            }
        }
        // shuffle the vec and treat it as a stack.
        removable_cells.shuffle(&mut rand::thread_rng());
        // sudoku to empty
        // let mut sudoku = Sudoku::rand_template();
        let mut sudoku = Sudoku::generate();
        while blanks > 0 {
            let (i, j) = match removable_cells.pop() {
                None => break,
                Some((i, j)) => (i, j),
            };
            let number_was = sudoku.grid[i][j].value;
            sudoku.grid[i][j].value = 0;

            if sudoku.is_solvable() {
                blanks -= 1;
                sudoku.grid[i][j].fixed = false;
            } else {
                sudoku.grid[i][j].value = number_was;
                // sudoku.grid[i][j].fixed = true;
            }
        }
        sudoku
    }

    pub fn state(&self) -> State {
        // values are appended to this vector from start to end
        let mut duplicates = Vec::new();
        // those are cleared each iteration
        let mut line_count = Vec::new();
        let mut col_count = Vec::new();
        let mut square_count = Vec::new();
        for number in 1..=9 {
            for i in 0..9 {
                // collect amount of repetitive numbers
                line_count.clear();
                col_count.clear();
                square_count.clear();
                for j in 0..9 {
                    if self.grid[i][j].get() == number {
                        line_count.push((i as u8, j as u8));
                    }
                    if self.grid[j][i].get() == number {
                        col_count.push((j as u8, i as u8));
                    }
                    let (i, j) = ((i / 3) * 3 + j / 3, (i % 3) * 3 + j % 3);
                    if self.grid[i][j].get() == number {
                        square_count.push((i as u8, j as u8));
                    }
                }
                // process this collections
                // if we have a repetition on line, column, square
                if line_count.len() > 1 {
                    duplicates.append(&mut line_count);
                }
                if col_count.len() > 1 {
                    duplicates.append(&mut col_count);
                }
                if square_count.len() > 1 {
                    duplicates.append(&mut square_count);
                }
            }
        }
        // if duplicates
        if duplicates.len() > 0 {
            State::Incorrect(duplicates)
        // else if there are no 0s -> finished
        } else if self
            .grid
            .iter()
            .all(|line| line.iter().all(|cell| cell.get() != 0))
        {
            State::FinishedCorrect
        // else unfinished
        } else {
            State::UnfinishedCorrect
        }
    }

    pub fn generate() -> Sudoku {
        let mut sudoku = Sudoku::empty();
        sudoku
            .set_line(0, [8, 3, 5, 4, 1, 6, 9, 2, 7])
            .expect("error 1 generate");
        sudoku
            .set_line(1, [2, 9, 6, 8, 5, 7, 4, 3, 1])
            .expect("error 2 generate");
        sudoku
            .set_line(2, [4, 1, 7, 2, 9, 3, 6, 5, 8])
            .expect("error 3 generate");
        sudoku
            .set_line(3, [5, 6, 9, 1, 3, 4, 7, 8, 2])
            .expect("error 3 generate");
        if let Some(mut sudoku) = sudoku.resolve_one() {
            sudoku.shuffle_numbers();
            sudoku.shuffle_rotate();
            for i in 0..9 {
                for j in 0..9 {
                    sudoku.grid[i][j].fix();
                }
            }
            sudoku
        } else {
            panic!("Zut ! La génération a raté !");
        }
    }

    fn shuffle_numbers(&mut self) {
        let perm = Perm::new(9);
        let rand_perm = perm.rand_perm();
        // println!("rand_perm: {:?}", rand_perm);
        for i in 0..9 as usize {
            for j in 0..9 as usize {
                self.grid[i][j].value = 1 + rand_perm[self.grid[i][j].value as usize - 1];
            }
        }
    }

    fn reverse_column(&mut self) {
        for i in 0..9 {
            for j in 0..5 {
                let a = self.grid[i][j].value;
                self.grid[i][j].value = self.grid[i][8 - j].value;
                self.grid[i][8 - j].value = a;
            }
        }
    }

    fn reverse_row(&mut self) {
        for i in 0..9 {
            for j in 0..5 {
                let a = self.grid[i][j].value;
                self.grid[i][j].value = self.grid[8 - i][j].value;
                self.grid[8 - i][j].value = a;
            }
        }
    }

    fn transpose(&mut self) {
        self.reverse_row();
        self.reverse_column();
    }

    fn shuffle_rotate(&mut self) {
        let rotations = rand::thread_rng().gen_range(0, 4);
        match rotations {
            1 => {
                self.transpose();
                self.reverse_row();
            }
            2 => {
                self.reverse_row();
                self.transpose();
            }
            3 => {
                self.transpose();
            }
            0 | _ => {}
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum State {
    UnfinishedCorrect,
    FinishedCorrect,
    Incorrect(Vec<(u8, u8)>),
}
