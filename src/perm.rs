// crates
use permutohedron;
use rand::{self, Rng};

// This struct uses the rand and permutohedron crates to generate a Vec of arrays of permutations.
pub struct Perm {
    permutations: Vec<Vec<u8>>,
}

impl Perm {
    pub fn new(n: u8) -> Perm {
        // Looking for a more idiomatic way of doing this
        let mut vector = Vec::new();
        for i in 0..n {
            vector.push(i);
        }
        let mut permutations = Vec::new();
        permutohedron::heap_recursive(&mut vector, |permutation| {
            permutations.push(permutation.to_vec());
        });
        Perm { permutations }
    }

    pub fn rand_perm(&self) -> &Vec<u8> {
        let random = rand::thread_rng().gen_range(0, self.permutations.len());
        self.permutations
            .get(random)
            .expect("Impossible out of range error in Perm::rand_perm")
    }
}

mod perm_tests {
    #[allow(unused_imports)]
    use super::Perm;

    #[test]
    fn check_size() {
        let p = Perm::new(3);
        assert_eq!(p.permutations.len(), 3 * 2);
        let p = Perm::new(5);
        assert_eq!(p.permutations.len(), 5 * 4 * 3 * 2);
        let p = Perm::new(9);
        assert_eq!(p.permutations.len(), 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2);
    }

    #[test]
    fn check_content() {
        let p = Perm::new(3);
        let pequal = vec![
            vec![0, 1, 2],
            vec![0, 2, 1],
            vec![1, 0, 2],
            vec![1, 2, 0],
            vec![2, 0, 1],
            vec![2, 1, 0],
        ];
        for vector in pequal {
            assert!(
                p.permutations.contains(&vector),
                "vector {:?} was not included",
                vector
            );
        }
    }
}
