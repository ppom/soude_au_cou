{ stdenv, fetchgit, fetchpatch, rustPlatform, cmake, perl, pkgconfig, zlib
, darwin, libiconv, installShellFiles
}:

with rustPlatform;

buildRustPackage rec {
  pname = "soude_au_cou";
  version = "0.1.0";
  url = "https://framagit.org/ppom/soude_au_cou.git";

  cargoSha256 = "017f5yczbglq0v0vwc4lg9yw254rfyrvf58ddmp4xqls97kzvzvi";

  src = fetchgit {
    url = url;
    rev = "v${version}";
    sha256 = "0d4barkpqf7h3a95ikfpihcfl03ahnrq71hfcmyav4hz0a0j0f7p";
  };

  outputs = [ "out" ];

  meta = with stdenv.lib; {
    description = "A Text User Interface to play sudoku on the terminal";
    homepage = url;
    license = licenses.blueOak100;
    maintainers = with maintainers; [];
  };
}
