# Soude au cou

This is a text user interface to play sudokus.

With colors and all! Zoom your terminal for better comfort.

## Install

You should use a Unix-like OS and have the Rust toolchain installed. See [here](https://rustup.rs/) for Rust installation instructions.

Just run `cargo build --release` and copy `./target/release/sudoku` somewhere in your `$PATH`.

Now play with `sudoku` !

## Commands

use `control c`, `control d` or `q` to quit.

use `hjkl` or `←↓↑→` to move.

use `space` or `0` to clear a cell.

use `123456789` to fill a cell.

AZERTY keyboards : you can type on the top keys `&é"'(-è_çà` as it were numbers. No need to press `shift`.

## Features

- pseudo random Sudoku generation
- different levels of difficulty
- warnings if the user tries to put 2 times the same number on the same square/line/column

## Roadmap

- correctly handle language : choose between french and english!
- write a `--help` handler
- save the sudoku on disk to permit to quit & reload a WIP sudoku.
- permit to suspend the program on `control z`
- permit to save suppositions for each cell, with `control 1`, `control 2`, etc.

## License

This software is licensed under the Blue Oak v1.0.0 license.
